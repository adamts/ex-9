<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","adamts","60998","adamts_basic");
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){// if request for id not empty else show all users
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM user WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result);
				$this->responseStatus = 200;
			} else {
			$sql = "select * FROM user";
			$usersSqlResult = mysqli_query($this ->_con, $sql);
			$users = [];
			while ($user = mysqli_fetch_array($usersSqlResult)){
					$users [] =["name"=> $user['name'], "email" => $user['email']];
			}	
			$this->response =array('result'=>$users);
			$this->responseStatus = 200;
			}
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->DBconnect();
	if (isset($this->request['params']['id'])) {
    if(empty($this->request['params']['id'])){
		$json = json_decode($this->request['params']['payload'],true);
		$name= $json['name'];
		$email= $json['email'];
		$sql ="INSERT INTO user (name, email) VALUES ('$name','$email')";
		$stmt = $this->_con->prepare($sql);
		$stmt->execute();
		$result = ['id'=>$this->_con->insert_id,'name'=>$name,'email'=>$email];//insert into id last id from sql
		$this->response = array('result' =>$result );
		$this->responseStatus = 200;
			}else{
						$this->response = array('result' =>'Wrong parameters for create new users' );
			$this->responseStatus = 200;
			}
		}else{ 
			$this->response = array('result' =>'Wrong parameters for create new users' );
			$this->responseStatus = 200;			
		}

	}

	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;

	}
}
